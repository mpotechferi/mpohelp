<div class="container">
  <div class="row">
    <div class="col-md-12 content-area">

    	
<h3 class="home-label"><?php echo lang("ctn_456") ?></h3>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("client/knowledge") ?>"><?php echo lang("ctn_456") ?></a></li>
  <li class="active"><?php echo $article->title ?></li>
</ol>

<div class="panel panel-default">
<div class="panel-heading"><?php echo $article->title ?></div>
<div class="panel-body">
<?php echo $article->body ?>
<p class="small-text"><?php echo lang("ctn_471") ?>: <?php echo date($this->settings->info->date_format, $article->last_updated_timestamp) ?></p>
</div>
</div>

</div>
</div>
</div>